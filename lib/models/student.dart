import 'package:final_exam/models/marks.dart';

class Student {
  String? studentCode;
  String? fullName;
  String? dateOfBirth;
  String? course;
  String? major;
  String? classNumber;
  Marks? marks;

  Student(
      {this.studentCode,
      this.fullName,
      this.dateOfBirth,
      this.course,
      this.major,
      this.classNumber,
      this.marks});

  Student.fromJson(Map<String, dynamic> json) {
    studentCode = json['studentCode'];
    fullName = json['fullName'];
    dateOfBirth = json['dateOfBirth'];
    course = json['course'];
    major = json['major'];
    classNumber = json['classNumber'];
    if (json["marks"] != null) {
      marks = Marks.fromJson(json["marks"]);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['studentCode'] = this.studentCode;
    data['fullName'] = this.fullName;
    data['dateOfBirth'] = this.dateOfBirth;
    data['course'] = this.course;
    data['major'] = this.major;
    data['classNumber'] = this.classNumber;
    if (this.marks != null) {
      data["marks"] = this.marks!.toJson();
    }
    return data;
  }

  bool isFullInformation() {
    if (studentCode == null ||
        fullName == null ||
        dateOfBirth == null ||
        course == null ||
        major == null ||
        classNumber == null) {
      return false;
    }
    return studentCode!.isNotEmpty &&
        fullName!.isNotEmpty &&
        dateOfBirth!.isNotEmpty &&
        course!.isNotEmpty &&
        major!.isNotEmpty &&
        classNumber!.isNotEmpty;
  }
}
