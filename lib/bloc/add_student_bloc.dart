import 'package:final_exam/bloc/base_bloc.dart';
import 'package:final_exam/models/student.dart';
import 'package:final_exam/services/student_services.dart';
import 'package:rxdart/subjects.dart';

class AddStudentBloC extends BaseBloC {
  final StudentServices studentServices;
  AddStudentBloC(this.studentServices);

  late Student _student;

  var _saveButtonObject = BehaviorSubject<bool>();

  Stream<bool> get saveButtonState => _saveButtonObject.stream;

  set studentCode(String value) {
    _student.studentCode = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  set fullName(String value) {
    _student.fullName = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  set dob(String value) {
    _student.dateOfBirth = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  set course(String value) {
    _student.course = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  set major(String value) {
    _student.major = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  set classNumber(String value) {
    _student.classNumber = value.trim();
    _saveButtonObject.add(_student.isFullInformation());
  }

  Future<bool> addStudent() async {
    showLoading();
    bool result = await studentServices.addStudent(_student);
    hideLoading();
    return result;
  }

  @override
  void clearData() {
    hideLoading();
    _saveButtonObject.add(false);
    _student = Student();
  }

  @override
  void dispose() {
    _saveButtonObject.close();
    super.dispose();
  }
}
