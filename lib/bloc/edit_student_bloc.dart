import 'package:final_exam/bloc/base_bloc.dart';
import 'package:final_exam/models/marks.dart';
import 'package:final_exam/models/student.dart';
import 'package:final_exam/services/student_services.dart';

class EditStudentBloC extends BaseBloC {
  final StudentServices studentServices;
  EditStudentBloC(this.studentServices);

  late Student _student;
  late Marks _marks;

  set student(Student value) {
    _student = value;
    _marks = _student.marks ?? Marks(0.0, 0.0, 0.0, 0.0);
  }

  set attendancePoints(double value) => _marks.attendancePoints = value;
  set testMarks1(double value) => _marks.testMarks1 = value;
  set testMarks2(double value) => _marks.testMarks2 = value;
  set finalExamMarks(double value) => _marks.finalExamMarks = value;

  Future<Student> updateStudent() async {
    if (!_marks.isValid()) {
      throw Exception('Điểm số sai định dạng.');
    }

    showLoading();
    _student.marks = _marks;
    Student result = await studentServices.editStudent(_student);
    hideLoading();
    return result;
  }

  @override
  void clearData() {
    hideLoading();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
