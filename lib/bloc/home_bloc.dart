import 'dart:async';

import 'package:final_exam/bloc/base_bloc.dart';
import 'package:final_exam/models/student.dart';
import 'package:final_exam/services/student_services.dart';
import 'package:rxdart/subjects.dart';

class HomeBloC extends BaseBloC {
  final StudentServices studentServices;
  HomeBloC(this.studentServices);

  Timer? _debounce;
  late List<Student> _listStudent;
  late String _searchKey;

  var _listStudentsObject = BehaviorSubject<List<Student>>();
  Stream<List<Student>> get listStudentStream => _listStudentsObject.stream;

  set searchKey(String key) {
    if (_debounce?.isActive ?? false) {
      _debounce?.cancel();
    }
    _debounce = Timer(const Duration(milliseconds: 500), () {
      _searchKey = key;
      _searchStudent();
    });
  }

  void _searchStudent() {
    List<Student> _result = _listStudent
        .where((element) =>
            element.fullName!.toLowerCase().contains(_searchKey.toLowerCase()))
        .toList();
    _listStudentsObject.add(_result);
  }

  Future<List<Student>> getListStudent() async {
    _listStudent = await studentServices.getListStudent();
    _listStudentsObject.add(_listStudent);
    return _listStudent;
  }

  void updateStudent(Student student) {
    List<Student> _currentList = _listStudentsObject.value;
    int index = _currentList
        .indexWhere((element) => element.studentCode == student.studentCode);
    _currentList[index] = student;
    _listStudent[_listStudent.indexWhere(
        (element) => element.studentCode == student.studentCode)] = student;
    _listStudentsObject.add(_currentList);
  }

  Future<bool> deleteStudent(Student student) async {
    bool deleteSuccess = await studentServices.deleteStudent(student);
    List<Student> _currentList = _listStudentsObject.value;
    _currentList
        .removeWhere((element) => element.studentCode == student.studentCode);
    _listStudent
        .removeWhere((element) => element.studentCode == student.studentCode);
    _listStudentsObject.add(_currentList);
    return deleteSuccess;
  }

  @override
  void clearData() {
    _searchKey = '';
  }

  @override
  void dispose() {
    _listStudentsObject.close();
    _debounce?.cancel();
    super.dispose();
  }
}
