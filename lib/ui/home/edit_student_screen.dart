import 'package:final_exam/bloc/edit_student_bloc.dart';
import 'package:final_exam/bloc/home_bloc.dart';
import 'package:final_exam/models/student.dart';
import 'package:final_exam/utils/app_color.dart';
import 'package:final_exam/utils/app_dialog.dart';
import 'package:final_exam/utils/app_text_style.dart';
import 'package:final_exam/utils/string_util.dart';
import 'package:final_exam/utils/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:provider/src/provider.dart';

class EditStudentScreen extends StatefulWidget {
  final Student student;
  const EditStudentScreen({Key? key, required this.student}) : super(key: key);

  @override
  _EditStudentScreenState createState() => _EditStudentScreenState();
}

class _EditStudentScreenState extends State<EditStudentScreen> {
  TextEditingController _attendancePointsController = TextEditingController();
  TextEditingController _testMarks1Controller = TextEditingController();
  TextEditingController _testMarks2Controller = TextEditingController();
  TextEditingController _finalExamMarksController = TextEditingController();
  FocusNode _testMarks1Node = FocusNode();
  FocusNode _testMarks2Node = FocusNode();
  FocusNode _finalExamMarksNode = FocusNode();
  late EditStudentBloC _editBloC;
  late Student student;

  @override
  void initState() {
    _editBloC = context.read<EditStudentBloC>();
    _editBloC.clearData();
    _editBloC.student = widget.student;
    student = widget.student;
    _attendancePointsController.text =
        '${student.marks?.attendancePoints ?? 0}';
    _testMarks1Controller.text = '${student.marks?.testMarks1 ?? 0}';
    _testMarks2Controller.text = '${student.marks?.testMarks2 ?? 0}';
    _finalExamMarksController.text = '${student.marks?.finalExamMarks ?? 0}';
    super.initState();
  }

  @override
  void dispose() {
    _attendancePointsController.dispose();
    _testMarks1Controller.dispose();
    _testMarks2Controller.dispose();
    _finalExamMarksController.dispose();
    _testMarks1Node.dispose();
    _testMarks2Node.dispose();
    _finalExamMarksNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Stack(
        children: [
          Scaffold(
            appBar: _buildAppBar(context),
            backgroundColor: Theme.of(context).backgroundColor,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          '''${student.fullName}''',
                          style: AppTextStyle.mediumBlack1A
                              .copyWith(fontSize: 16.0),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 4.0),
                        Text(
                          'Mã sinh viên: ${student.studentCode}',
                          style: AppTextStyle.regularBlack1A
                              .copyWith(fontSize: 16.0),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 4.0),
                        _buildRichText(context,
                            text: "Khoá: ${student.course} - ${student.major}",
                            highlightText:
                                ' - Nhóm lớp ${student.classNumber}'),
                        SizedBox(height: 4.0),
                        Text(
                          'Ngày sinh: ${student.dateOfBirth}',
                          style: AppTextStyle.regularBlack1A
                              .copyWith(fontSize: 16.0),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 18.0),
                        textField(
                          context,
                          controller: _attendancePointsController,
                          labelText: 'Điểm chuyên cần',
                          keyboardType: TextInputType.number,
                          maxLength: 4,
                          onChanged: (value) => _editBloC.attendancePoints =
                              StringUtil.doubleFromString(value),
                          onSubmitted: (value) {
                            _testMarks1Node.requestFocus();
                          },
                        ),
                        SizedBox(height: 18.0),
                        textField(
                          context,
                          controller: _testMarks1Controller,
                          focusNode: _testMarks1Node,
                          labelText: 'Kiểm tra 1',
                          keyboardType: TextInputType.number,
                          maxLength: 4,
                          onChanged: (value) => _editBloC.testMarks1 =
                              StringUtil.doubleFromString(value),
                          onSubmitted: (value) {
                            _testMarks2Node.requestFocus();
                          },
                        ),
                        SizedBox(height: 18.0),
                        textField(
                          context,
                          controller: _testMarks2Controller,
                          focusNode: _testMarks2Node,
                          labelText: 'Kiểm tra 2',
                          keyboardType: TextInputType.number,
                          maxLength: 4,
                          onChanged: (value) => _editBloC.testMarks2 =
                              StringUtil.doubleFromString(value),
                          onSubmitted: (value) {
                            _finalExamMarksNode.requestFocus();
                          },
                        ),
                        SizedBox(height: 18.0),
                        textField(
                          context,
                          controller: _finalExamMarksController,
                          focusNode: _finalExamMarksNode,
                          labelText: 'Thi học kỳ',
                          keyboardType: TextInputType.number,
                          maxLength: 4,
                          onChanged: (value) => _editBloC.finalExamMarks =
                              StringUtil.doubleFromString(value),
                          onSubmitted: (value) {},
                        ),
                        SizedBox(height: 18.0),
                      ],
                    ),
                  ),
                ),
                _saveButton(context),
              ],
            ),
          ),
          _loadingState(context),
        ],
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).backgroundColor,
      leading: BackButton(),
      title: Text(
        'Thông tin học sinh',
        style: AppTextStyle.mediumBlack1A.copyWith(fontSize: 18),
      ),
      centerTitle: true,
    );
  }

  Widget _buildRichText(BuildContext context,
      {required String text, required String highlightText}) {
    return RichText(
      text: TextSpan(
        text: text,
        style: AppTextStyle.regularBlack1A.copyWith(fontSize: 16.0),
        children: [
          TextSpan(
            text: highlightText,
            style: AppTextStyle.mediumBlack1A.copyWith(fontSize: 16.0),
          )
        ],
      ),
    );
  }

  Widget _saveButton(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      color: AppColor.colorWhite,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        disabledColor: AppColor.colorGrey97,
        minWidth: double.infinity,
        height: 54,
        color: AppColor.colorDarkBlue,
        onPressed: updateStudent,
        child: Text(
          'Cập nhật',
          style: TextStyle(
            color: AppColor.colorWhite,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        padding: EdgeInsets.all(0),
      ),
    );
  }

  Widget _loadingState(BuildContext context) {
    return StreamBuilder<bool>(
        stream: _editBloC.loadingState,
        builder: (_, snapshot) {
          bool isLoading = snapshot.data ?? false;
          if (isLoading) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              color: AppColor.colorGrey97.withOpacity(0.5),
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
          return SizedBox.shrink();
        });
  }

  void updateStudent() {
    WidgetsBinding.instance?.focusManager.primaryFocus?.unfocus();
    _editBloC.updateStudent().then((newStudent) {
      context.read<HomeBloC>().updateStudent(newStudent);
      showCupertinoDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return succesfulMessageDialog(context, content: 'Cập nhật');
        },
      ).then((value) => Navigator.pop(context));
    }).catchError((error) {
      _editBloC.hideLoading();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(StringUtil.stringFromException(error))));
    });
  }
}
