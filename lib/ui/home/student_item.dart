import 'package:final_exam/bloc/home_bloc.dart';
import 'package:final_exam/models/student.dart';
import 'package:final_exam/ui/home/edit_student_screen.dart';
import 'package:final_exam/utils/app_color.dart';
import 'package:final_exam/utils/app_dialog.dart';
import 'package:final_exam/utils/app_text_style.dart';
import 'package:final_exam/utils/string_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class StudentItem extends StatefulWidget {
  final Student student;
  const StudentItem({Key? key, required this.student}) : super(key: key);

  @override
  _StudentItemState createState() => _StudentItemState();
}

class _StudentItemState extends State<StudentItem> {
  late Student student;
  @override
  void initState() {
    student = widget.student;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.only(bottom: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color(0xff141A1A1A),
            blurRadius: 32,
            offset: Offset(0, 8),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  '''${student.fullName}''',
                  style: AppTextStyle.mediumBlack1A.copyWith(fontSize: 16),
                  textAlign: TextAlign.left,
                ),
              ),
              _buildButton(context, 'Sửa', editStudent,
                  color: AppColor.colorGreen),
              SizedBox(width: 5.0),
              _buildButton(context, 'Xoá', deleteStudent,
                  color: AppColor.colorRed),
            ],
          ),
          SizedBox(height: 10.0),
          Text(
            'Mã sinh viên: ${student.studentCode}',
            style: AppTextStyle.regularBlack1A,
            textAlign: TextAlign.left,
          ),
          _buildRichText(context,
              text: "Khoá: ${student.course} - ${student.major}",
              highlightText: ' - Nhóm lớp ${student.classNumber}'),
          Text(
            'Ngày sinh: ${student.dateOfBirth}',
            style: AppTextStyle.regularBlack1A,
            textAlign: TextAlign.left,
          ),
          _buildRichText(context,
              text: "Điểm chuyên cần: ",
              highlightText: ' ${student.marks?.attendancePoints ?? ''}'),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: _buildRichText(context,
                    text: "Kiểm tra 1: ",
                    highlightText: ' ${student.marks?.testMarks1 ?? ''}'),
              ),
              Flexible(
                flex: 1,
                child: _buildRichText(context,
                    text: "Kiểm tra 2: ",
                    highlightText: ' ${student.marks?.testMarks2 ?? ''}'),
              ),
            ],
          ),
          _buildRichText(context,
              text: "Thi học kỳ: ",
              highlightText: ' ${student.marks?.finalExamMarks ?? ''}'),
          Text(
            'Điểm trung bình: ${student.marks != null ? student.marks!.getGPA().toStringAsFixed(2) : 0.0}',
            style: AppTextStyle.mediumBlack1A.copyWith(fontSize: 16),
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }

  Widget _buildRichText(BuildContext context,
      {required String text, required String highlightText}) {
    return RichText(
      text: TextSpan(
        text: text,
        style: AppTextStyle.regularBlack1A,
        children: [
          TextSpan(
            text: highlightText,
            style: AppTextStyle.mediumBlack1A,
          )
        ],
      ),
    );
  }

  Widget _buildButton(
      BuildContext context, String label, void Function()? onPressed,
      {Color color = AppColor.colorGreen}) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        primary: color,
        elevation: 0.0,
      ),
      child: Text(label),
    );
  }

  void editStudent() {
    Navigator.push(context,
        MaterialPageRoute(builder: (_) => EditStudentScreen(student: student)));
  }

  void deleteStudent() {
    showCupertinoDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return confirmDialog(
            context, 'Xoá', 'Bạn chắc chắn muốn xoá học sinh này?');
      },
    ).then((acceptDelete) {
      if (acceptDelete ?? false) {
        context.read<HomeBloC>().deleteStudent(student).catchError((error) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(StringUtil.stringFromException(error))));
        });
      }
    });
  }
}
